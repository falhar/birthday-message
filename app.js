const express = require('express');
const cron = require('cron');
const cors = require('cors');
const moment = require('moment-timezone');
const { find } = require('geo-tz');
const { Op } = require('sequelize');
const config = require('./config');
const morgan = require('./tools/morgan');
const userRouter = require('./routes/user.route');
const ApiError = require('./errors/api-error');
const errorMiddleware = require('./middlewares/error');
const sendMessageQueue = require('./services/queue');
const { user, Sequelize } = require('./models');

const app = express();

if (config.app.env !== 'test') {
  app.set('trust proxy', true);
  app.use(morgan.logSuccessRequest);
  app.use(morgan.logErrorRequest);
}

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

app.use('/user', userRouter);

cron.job(
  '1 0 0 * * *',
  async () => {
    const listBirthday = await user.findAll({
      where: {
        [Op.and]: [
          Sequelize.where(Sequelize.fn('month', Sequelize.col('birthdate')), moment().tz('Pacific/Fakaofo').month() + 1),
          Sequelize.where(Sequelize.fn('day', Sequelize.col('birthdate')), moment().tz('Pacific/Fakaofo').date()),
        ],
      },
    });
    if (listBirthday.length !== 0) {
      listBirthday.map(async (userBirthday) => {
        const [lat, long] = userBirthday.location.split(',');
        const [userTimezone] = find(lat, long);
        // eslint-disable-next-line no-param-reassign
        userBirthday.dataValues.message = `Hey ${userBirthday.firstName} ${userBirthday.lastName} it's your birthday`;
        sendMessageQueue.add(`job${userBirthday.id}`, userBirthday, {
          removeOnComplete: true,
          repeat: { cron: '0 9 * * *', tz: userTimezone },
        });
      });
    }
  },
  null,
  true,
  'Pacific/Fakaofo'
);

app.use(async (req, res, next) => next(new ApiError(404, 'Endpoint Not Found!')));

app.use(errorMiddleware);

module.exports = app;
