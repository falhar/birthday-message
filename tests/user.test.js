const request = require('supertest');
const moment = require('moment-timezone');
const { faker } = require('@faker-js/faker');
const app = require('../app');
const db = require('../models');

describe('User route', () => {
  beforeEach(async () => {
    jest.clearAllMocks();
    await db.sequelize.sync({ force: true });
  });

  describe('POST /user', () => {
    test('It should create user', async () => {
      const res = await request(app)
        .post('/user')
        .send({
          firstName: faker.name.firstName(),
          lastName: faker.name.lastName(),
          birthdate: moment(faker.date.past()).format('YYYY-MM-DD'),
          location: `${faker.address.latitude()},${faker.address.longitude()}`,
        })
        .expect(201);
      expect(res.body).toMatchObject(
        expect.objectContaining({
          status: 'Success',
          data: expect.objectContaining({
            id: expect.any(Number),
            firstName: expect.any(String),
            lastName: expect.any(String),
            birthdate: expect.any(String),
            location: expect.any(String),
          }),
        })
      );
    });
  });

  describe('PUT /user', () => {
    test('It should edit user', async () => {
      const oldData = {
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
        birthdate: moment(faker.date.past()).format('YYYY-MM-DD'),
        location: `${faker.address.latitude()},${faker.address.longitude()}`,
      };
      const userToUpdate = await db.user.create(oldData);
      const res = await request(app)
        .put(`/user/${userToUpdate.id}`)
        .send({
          firstName: faker.name.firstName(),
          lastName: faker.name.lastName(),
          birthdate: moment(faker.date.past()).format('YYYY-MM-DD'),
          location: `${faker.address.latitude()},${faker.address.longitude()}`,
        })
        .expect(200);
      expect(res.body).toMatchObject(
        expect.objectContaining({
          status: 'Success',
        })
      );
    });
  });

  describe('DELETE /user', () => {
    test('It should delete user', async () => {
      const oldData = {
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
        birthdate: moment(faker.date.past()).format('YYYY-MM-DD'),
        location: `${faker.address.latitude()},${faker.address.longitude()}`,
      };
      const userToDelete = await db.user.create(oldData);
      const res = await request(app).delete(`/user/${userToDelete.id}`).expect(200);
      expect(res.body).toMatchObject(expect.objectContaining({ status: 'Success' }));
    });

    test('Error user not found', async () => {
      const res = await request(app).delete(`/user/1`).expect(422);
      expect(res.body).toMatchObject(
        expect.objectContaining({
          errors: expect.objectContaining({ id: expect.objectContaining({ message: 'User Not Found!' }) }),
          message: 'Validation Error',
          status: 'Error',
          stack: expect.any(String),
        })
      );
    });
  });
});
