const { find } = require('geo-tz');
const moment = require('moment-timezone');
const ApiError = require('../errors/api-error');
const { user } = require('../models');
const sendMessageQueue = require('../services/queue');

class UserControllers {
  async createUserCon(req, res) {
    const { firstName, lastName, birthdate, location } = req.body;
    const data = await user.create({ firstName, lastName, birthdate, location });
    const [lat, long] = location.split(',');
    const [userTimezone] = find(lat, long);
    if (
      moment(birthdate).format('MM-DD') === moment().tz(userTimezone).format('MM-DD') &&
      moment().tz(userTimezone).format('HH') < 9
    ) {
      const userBirthday = {
        id: data.id,
        firstName,
        lastName,
        birthdate,
        location,
        message: `Hey ${firstName} ${lastName} it's your birthday`,
      };

      sendMessageQueue.add(`job${data.id}`, userBirthday, {
        removeOnComplete: true,
        repeat: { cron: '0 9 * * *', tz: userTimezone },
      });
    }
    res.status(201).json({
      status: 'Success',
      data,
    });
  }

  async updateUserCon(req, res) {
    const { firstName, lastName, birthdate, location } = req.body;
    const updated = await user.update({ firstName, lastName, birthdate, location }, { where: { id: req.params.id } });
    if (!updated[0]) {
      throw new ApiError(500, 'Request Failed');
    }
    const [lat, long] = location.split(',');
    const [userTimezone] = find(lat, long);
    if (
      moment(birthdate).format('MM-DD') === moment().tz(userTimezone).format('MM-DD') &&
      moment().tz(userTimezone).format('HH') < 9
    ) {
      const userBirthday = {
        id: req.params.id,
        firstName,
        lastName,
        birthdate,
        location,
        message: `Hey ${firstName} ${lastName} it's your birthday`,
      };

      sendMessageQueue.add(`job${req.params.id}`, userBirthday, {
        removeOnComplete: true,
        repeat: { cron: '0 9 * * *', tz: userTimezone },
      });
    }
    res.status(200).json({
      status: 'Success',
    });
  }

  async delUserCon(req, res) {
    const checkUser = await user.findByPk(req.params.id);
    const del = await user.destroy({ where: { id: req.params.id } });
    if (!del) {
      throw new ApiError(500, 'Request Failed');
    }
    const [lat, long] = checkUser.location.split(',');
    const [userTimezone] = find(lat, long);
    if (
      moment(checkUser.birthdate).format('MM-DD') === moment().tz(userTimezone).format('MM-DD') &&
      moment().tz(userTimezone).format('HH') < 9
    ) {
      const jobtoremove = await sendMessageQueue.getDelayed();
      jobtoremove.map(async (dt) => {
        if (dt.name === `job${req.params.id}`) {
          await sendMessageQueue.removeJobs(dt.id);
        }
      });
    }
    res.status(200).json({
      status: 'Success',
    });
  }
}

module.exports = new UserControllers();
