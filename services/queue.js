const Bull = require('bull');
const axios = require('axios');
const config = require('../config');
const logger = require('../tools/logger');

const sendMessageQueue = new Bull('sendMessageQueue');

sendMessageQueue.process('*', async (job, done) => {
  try {
    logger.info(`job ${job.name} sent`);
    await axios.post(`https://hookb.in/${config.hookbin.path}`, { message: job.data.message });
    done();
  } catch (error) {
    done(new Error(error.message));
  }
});

sendMessageQueue.on('completed', async (job) => {
  await sendMessageQueue.removeRepeatableByKey(job.opts.repeat.key);
  logger.info(`${job.name} has been complete`);
  const jobtoremove = await sendMessageQueue.getFailed();
  jobtoremove.map(async (dt) => {
    await sendMessageQueue.removeJobs(dt.id);
  });
});
sendMessageQueue.on('failed', async (job, err) => {
  logger.info(`${job.name} failed to process`);
  logger.error(`caused by ${err.message}`);
});
sendMessageQueue.on('error', (err) => logger.error(`err ${err.message}`));

module.exports = sendMessageQueue;
