const ApiError = require('../../errors/api-error');
const myValidationResult = require('../../services/myValidationResult');

function handleValidationError(req, res, next) {
  const errors = myValidationResult(req);
  if (!errors.isEmpty()) {
    next(new ApiError(422, 'Validation Error', errors.mapped({})));
  } else {
    next();
  }
}

module.exports = handleValidationError;
