const { body, param } = require('express-validator');
const { user } = require('../../models');
const handleValidationError = require('./validator');

module.exports = {
  bodyUserVal: [
    body('firstName').notEmpty().withMessage('firstName required').isString(),
    body('lastName').isString().optional(),
    body('birthdate').notEmpty().withMessage('birthdate required').toDate().isISO8601(),
    body('location')
      .notEmpty()
      .withMessage('location required')
      .isLatLong()
      .withMessage('format string "latitude,longitude"'),
    handleValidationError,
  ],
  paramUserId: [
    param('id')
      .notEmpty()
      .withMessage('id required')
      .custom(async (id) => {
        const checkUser = await user.count({ where: { id } });
        if (!checkUser) {
          throw new Error('User Not Found!');
        }
        return true;
      }),
    handleValidationError,
  ],
};
