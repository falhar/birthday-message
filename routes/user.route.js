const router = require('express').Router();
const { createUserCon, updateUserCon, delUserCon } = require('../controllers/user.controller');
const { bodyUserVal, paramUserId } = require('../middlewares/validators/user.validator');
const catchAsync = require('./catch-async');

router.post('/', bodyUserVal, catchAsync(createUserCon));
router.put('/:id', paramUserId, bodyUserVal, catchAsync(updateUserCon));
router.delete('/:id', paramUserId, catchAsync(delUserCon));

module.exports = router;
