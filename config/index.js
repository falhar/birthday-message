const path = require('path');
require('dotenv').config({ path: path.join(__dirname, '../.env') });
const db = require('./db');

module.exports = {
  app: {
    port: process.env.PORT,
    env: process.env.NODE_ENV,
  },
  db,
  hookbin: {
    path: process.env.HOOKBIN_PATH,
  },
};
